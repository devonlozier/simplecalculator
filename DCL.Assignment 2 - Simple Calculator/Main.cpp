/*
Devon Lozier
50572 - Ryan Appel
Sept 5th 2021
*/


#include <iostream> // _getch();
#include <conio.h> // console in out

using namespace std;


// function prototype
float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num1, float num2, float& answer);
float Pow(float, int);


int main() // entry point
{
	string operation;
	float answer = 0;
	float num1;
	float num2;
	string response;
	bool a = true;

	while(true)
	{ 
		cout << "Please enter a number, an operation modifier (+, -, *, /, ^), and another number." << "\n";
		cin >> num1 >> operation >> num2;

		if (operation == "+")
		{
			cout << Add(num1, num2) << "\n";
		}
		else if (operation == "-")
		{
			cout << Subtract(num1, num2) << "\n";
		}
		else if (operation == "*")
		{
			cout << Multiply(num1, num2) << "\n";
		}
		else if (operation == "/")
		{
			if (Divide(num1, num2, answer))
			{
				cout << answer << "\n";
			}
			else
			{
				cout << "Please enter a non zero denominator." << "\n";
			}
		}
		else if (operation == "^")
		{
			cout << Pow(num1, num2) << "\n";
		}

		cout << "Enter N to close, press any other key to continue." << "\n";
		cin >> response;

		if (response == "N" || response == "n")
		{
			break;
		}
	}
}

float Add(float num1, float num2)
{
	return num1 + num2;
}

float Subtract(float num1, float num2)
{
	return num1 - num2;
}

float Multiply(float num1, float num2)
{
	return num1 * num2;
}

bool Divide(float num1, float num2, float &answer)
{
	if (num2 != 0)
	{
		answer = num1 / num2;

		return true;
	}
	else
	{
		return false;
	}
}

float Pow(float num1, int num2)
{
	if (num2 >= 2)
	{
		return Pow(num1, num2 - 1) * num1;
	}
	else if (num2 == 1)
	{
		return num1;
	}
	else if (num2 == 0)
	{
		return 1;
	}
}